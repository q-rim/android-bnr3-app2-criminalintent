package com.q_rim.app2;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

public class CrimeLab {
  private ArrayList<Crime> crimes;

  private static CrimeLab crimeLab;
  private Context appContext;

  private CrimeLab(Context appContext) {
    appContext = appContext;
    crimes = new ArrayList<Crime>();

    // populating the array list with 100 boring Crime objects.
    for (int i = 0; i<100; i++) {
      Crime c = new Crime();
      c.setTitle("Crime #" + i);
      c.setSolved(i % 2 == 0);      // Every other one
      crimes.add(c);
    }
  }

  public static CrimeLab get(Context c) {
    if (crimeLab == null) {
      crimeLab = new CrimeLab(c.getApplicationContext());
    }
    return crimeLab;
  }

  public ArrayList<Crime> getCrimes() {
    return crimes;
  }

  public Crime getCrime(UUID id) {
    for (Crime c : crimes) {
      if (c.getId().equals(id)) {
        return c;
      }
    }
    return null;
  }
}
