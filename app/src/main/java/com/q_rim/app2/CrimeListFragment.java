package com.q_rim.app2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CrimeListFragment extends ListFragment{
  private static final String TAG = "----Q----";
  private ArrayList<Crime> crimes;

  // Getting Argument Back from Activity2
  @Override
  public void onResume() {
    super.onResume();
    ((CrimeAdapter)getListAdapter()).notifyDataSetChanged();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getActivity().setTitle(R.string.crime_title_label);
    crimes = CrimeLab.get(getActivity()).getCrimes();

    //ArrayAdapter<Crime> adapter = new ArrayAdapter<Crime>(getActivity(), android.R.layout.simple_list_item_1, crimes);      // Use this for ArrayAdaptor<T> method
    CrimeAdapter adapter = new CrimeAdapter(crimes);                                                                          // Use this for CrimeAdaptor method

    // this method is a ListFragment convenience method that you can use to set the adaptor of the implicit ListView managed by CrimeListFragemnt.
    setListAdapter(adapter);
  }

  // Responding to ListView touch
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    //Crime c = (Crime)(getListAdapter()).getItem(position);                                                                  // Use this for ArrayAdaptor<T> method
    Crime c = ((CrimeAdapter)getListAdapter()).getItem(position);                                                             // Use this for CrimeAdaptor method
    Log.d(TAG, c.getTitle() + " was clicked");   Toast.makeText(getActivity().getApplicationContext(), c.getTitle() + " was clicked", Toast.LENGTH_SHORT).show();

    // Start CrimeActivity
    Intent i = new Intent(getActivity(), CrimeActivity.class);
    i.putExtra(CrimeFragment.EXTRA_KEY_CRIME_ID, c.getId());
    startActivity(i);
  }

  // Adding custom adapter as inner class
  private class CrimeAdapter extends ArrayAdapter<Crime> {

    public CrimeAdapter(ArrayList<Crime> crimes) {
      super(getActivity(), 0, crimes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // If View is not given, inflate one.
      if (convertView == null) {
        convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_crime, null);
      }

      // Configure the view for this Crime
      Crime c = getItem(position);

      TextView titleTextView = (TextView)convertView.findViewById(R.id.crime_list_item_title_TextView);
      titleTextView.setText(c.getTitle());

      TextView dateTextView = (TextView)convertView.findViewById(R.id.crime_list_item_date_TextView);
      dateTextView.setText(c.getDate());

      CheckBox solvedCheckBox = (CheckBox)convertView.findViewById(R.id.crime_list_item_solved_CheckBox);
      solvedCheckBox.setChecked(c.isSolved());

      return convertView;
    }
  }
}
