package com.q_rim.app2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

public class CrimeFragment extends Fragment{

  public static final String EXTRA_KEY_CRIME_ID = "com.q_rim.app2.crime_id";

  private Crime crime;
  private EditText titleField;
  private Button dateButton;
  private CheckBox solvedCheckBox;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //UUID crimeId = (UUID)getActivity().getIntent().getSerializableExtra(EXTRA_KEY_CRIME_ID);      // Used for direct retrieval of data
    UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_KEY_CRIME_ID);                        // Used for retrieving Arguments <Listing 10.7>
    crime = CrimeLab.get(getActivity()).getCrime(crimeId);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_crime, parent, false);

    titleField = (EditText)v.findViewById(R.id.crime_title_EditText);
    titleField.setText(crime.getTitle());
    titleField.addTextChangedListener(new TextWatcher() {
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        crime.setTitle(s.toString());
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    dateButton = (Button)v.findViewById(R.id.crimeDate_Button);
    dateButton.setText(crime.getDate());
    dateButton.setEnabled(false);

    solvedCheckBox = (CheckBox)v.findViewById(R.id.crimeSolved_CheckBox);
    solvedCheckBox.setChecked(crime.isSolved());
    solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        crime.setSolved(isChecked);
      }
    });

    return v;
  }

  // Used for Fragment Argument Retrieval
  public static CrimeFragment newInstance(UUID crimeId) {
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_KEY_CRIME_ID, crimeId);

    CrimeFragment fragment = new CrimeFragment();
    fragment.setArguments(args);

    return fragment;
  }
}