package com.q_rim.app2;

import java.util.Date;
import java.util.UUID;

public class Crime {
  private UUID id;
  private String title;
  private Date date;
  private boolean solved;

  public Crime() {
    // Generate unique identifier
    id = UUID.randomUUID();
    date = new Date();
  }

  // Added this to make the Title not return some memory location.
  @Override
  public String toString() {
    return title;
  }

  public UUID getId() {
    return id;
  }

  public String getDate() {
    String[] dateSplit = date.toString().split("\\s+");    // split date components.
    String[] timeSplit = dateSplit[3].split(":");           // change HH:MM:SS  into  HH:MM
    // Stitch together the values
    String d = dateSplit[0] +", "+ dateSplit[1] +" "+ dateSplit[2] + ", " + dateSplit[5] + "    " + timeSplit[0] + ":" + timeSplit[1];
    return d;
  }

  public boolean isSolved() {
    return solved;
  }

  public void setSolved(boolean solved) {
    this.solved = solved;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}