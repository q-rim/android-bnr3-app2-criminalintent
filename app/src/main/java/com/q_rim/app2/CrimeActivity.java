package com.q_rim.app2;

import android.support.v4.app.Fragment;

import java.util.UUID;

public class CrimeActivity extends SingleFragmentActivity {

  @Override
  protected Fragment createFragment() {

    // Retrieval using Fragment Arguments
    UUID crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_KEY_CRIME_ID);
    return CrimeFragment.newInstance(crimeId);

    // Direct Retrieval
    //return new CrimeFragment();
  }
}

